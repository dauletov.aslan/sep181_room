package kz.astana.roomapp;

import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    private ArrayAdapter<String> adapter;
    private AppDatabase database;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        database = App.instance.getDatabase();

        ListView listView = findViewById(R.id.listView);
        adapter = new ArrayAdapter<>(MainActivity.this, android.R.layout.simple_list_item_1, new ArrayList<>());
        listView.setAdapter(adapter);

        new DatabaseTask().execute();
    }

    private class DatabaseTask extends AsyncTask<Void, Void, List<Employee>> {
        @Override
        protected List<Employee> doInBackground(Void... voids) {
            EmployeeDao employeeDao = database.employeeDao();

            Employee employee = new Employee("Aslan", 200);
            employee.id = 2;
            employee.name = "Vasya";
            employee.salary = 1000;

            employeeDao.insert(employee);

            return employeeDao.getAll();
        }

        @Override
        protected void onPostExecute(List<Employee> employees) {
            super.onPostExecute(employees);
            ArrayList<String> list = new ArrayList<>();
            for (Employee e : employees) {
                list.add(e.name);
            }
            adapter.addAll(list);
        }
    }
}