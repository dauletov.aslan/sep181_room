package kz.astana.roomapp;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "employees")
public class Employee {
    public Employee(String name, int salary) {
        this.name = name;
        this.salary = salary;
    }

    @PrimaryKey(autoGenerate = true)
    public long id;
    public String name;
    public int salary;
}