package kz.astana.roomapp;

import java.util.List;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import static androidx.room.OnConflictStrategy.REPLACE;

@Dao
public interface EmployeeDao {
    @Query("SELECT * FROM employees")
    List<Employee> getAll();

    @Query("SELECT * FROM employees WHERE id = :id")
    Employee getById(long id);

    @Insert(onConflict = REPLACE)
    void insert(Employee employee);

    @Insert
    void insert(Employee... employees);

    @Update
    void update(Employee employee);

    @Delete
    void delete(Employee employee);
}
